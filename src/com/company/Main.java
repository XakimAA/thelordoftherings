package com.company;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

import middleearth.*;
import middleearth.lightArmy.Human;


public class Main {

    private static void printPreviousArmy(){
        try {
            FileInputStream fis = new FileInputStream("army.out");
            ObjectInputStream oin = new ObjectInputStream(fis);
            ArrayList<MiddleEarthCitizen> previousArmy = (ArrayList<MiddleEarthCitizen>) oin.readObject();
            System.out.println("Победители прошлой битвы:");
            War.outArmy(previousArmy);
        }catch (Exception e){
            System.out.println("Данных о предыдущих сражениях нет");
        }
    }

    private static void writeFile(ArrayList<MiddleEarthCitizen> army){
        try {
            FileOutputStream fos = new FileOutputStream("army.out");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(army);
            oos.flush();
            oos.close();
        }
        catch (IOException e){
            System.out.println("Произошла ошибка");
            System.out.println(e.toString());
        }
    }

    public static void main(String[] args) {
        printPreviousArmy();
        ArrayList<MiddleEarthCitizen> lightArmy = War.createLightArmy();
        War.outArmy(lightArmy);
        ArrayList<MiddleEarthCitizen> darkArmy = War.createDarkArmy();
        War.outArmy(darkArmy);
        System.out.println();
        War.roundOne(lightArmy,darkArmy, a->a.getRider(),"В первом");
        War.roundOne(lightArmy,darkArmy, a->(!a.getRider()),"Во втором");
        War.roundOne(lightArmy,darkArmy, null,"В третьем");
        if (lightArmy.size()==0){
            System.out.println("В итоге победила темная армия!");
            writeFile(darkArmy);
        } else {
            System.out.println("В итоге победила светлая армия!");
            writeFile(lightArmy);
        }
    }
}
