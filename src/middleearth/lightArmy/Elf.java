package middleearth.lightArmy;

import middleearth.MiddleEarthCitizen;

/**
 * Created by Анжелика on 25.10.2018.
 */
public class Elf extends MiddleEarthCitizen {

    public Elf(){
        super(random.nextInt(4)+4, "Elf" + random.nextInt(99),false);
    }

    public Elf(int power,String name,boolean isRider){
        super(power,name,isRider);
    }
}
