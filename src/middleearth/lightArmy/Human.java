package middleearth.lightArmy;

import middleearth.MiddleEarthCitizen;

/**
 * Created by Анжелика on 30.09.2018.
 */
public class Human extends MiddleEarthCitizen {

    public Human (int power,String name,boolean isRider){
        super(power,name,isRider);
    }

    public Human(){
        super(random.nextInt(2)+7,"Human" +String.valueOf(random.nextInt(99)),false);
    }

}
