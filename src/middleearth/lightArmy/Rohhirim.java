package middleearth.lightArmy;

/**
 * Created by Анжелика on 30.09.2018.
 */
public class Rohhirim extends Human{
    public Horse horse;

    public Rohhirim(){
        super(random.nextInt(2)+7,"Rohhirim" +String.valueOf(random.nextInt(99)),true);
        horse = new Horse();
    }

    public Rohhirim(int power, String name, boolean isRider){
        super(power,name,isRider);
        horse = new Horse();
    }

    @Override
    public void reducePower(int value){
        if (value > horse.getPower()){
            value = value - horse.getPower();
            horse.setPower(0);
            setPower(value > super.getPower() ? 0: super.getPower()-value );
        }
        else
            horse.setPower(horse.getPower() - value);
    }

    @Override
    public int getPower() {
        return super.getPower() + horse.getPower();
    }

    @Override
    public void setPower(int power) {
        super.setPower(power);
    }

    @Override
    public String toString() {
        return super.toString() + " Horse: " + horse.toString();
    }
}
