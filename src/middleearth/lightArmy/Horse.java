package middleearth.lightArmy;

import java.io.Serializable;
import java.util.Random;

/**
 * Created by Анжелика on 25.10.2018.
 */
public class Horse implements Serializable{
    int power;
    String name;

    public void setPower(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Horse(){
        Random random = new Random();
        this.power = random.nextInt(2) + 4;
        this.name = "Horse" + String.valueOf(random.nextInt(99) + 1);
    }

    public Horse(int power,String name){
        this.power = power;
        this.name = name;
    }

    public String toString() {
        return "name = " + this.name + " power = " + this.power;
    }
}
