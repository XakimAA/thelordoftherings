package middleearth.lightArmy;

import middleearth.MiddleEarthCitizen;

/**
 * Created by Анжелика on 25.10.2018.
 */
public class Wizard extends MiddleEarthCitizen {
    public Horse horse;

    public Wizard(){
        super(20 ,"Wizard" + random.nextInt(99),true);
        horse = new Horse();
    }

    public Wizard(int power, String name){
        super(power,name,true);
        horse = new Horse();
    }

    @Override
    public void reducePower(int value){
        if (value > horse.getPower()){
            value = value - horse.getPower();
            horse.setPower(0);
            setPower(value > super.getPower() ? 0: super.getPower()-value );
        }
        else
            horse.setPower(horse.getPower() - value);
    }

    @Override
    public int getPower() {
        return super.getPower() + horse.getPower();
    }

    @Override
    public String toString() {
        return super.toString() + " Horse: " + horse.toString();
    }
}
