package middleearth.lightArmy;

import middleearth.MiddleEarthCitizen;

/**
 * Created by Анжелика on 25.10.2018.
 */
public class WoodenElf extends Elf {
    public WoodenElf(){
        super(6, "WoodenElf" + MiddleEarthCitizen.random.nextInt(99),false);
    }

    public WoodenElf(int power,String name){
        super(power,name,false);
    }
}
