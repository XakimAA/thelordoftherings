package middleearth;

import middleearth.darkArmy.*;
import middleearth.lightArmy.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Анжелика on 25.10.2018.
 */
public class War {

    private static final int MAX_PLAYER = 7;
    private static final int MIN_PLAYER = 4;
    private static final Random random = new Random();

    private static int getCountPlayer(){
        return random.nextInt(MAX_PLAYER-MIN_PLAYER+1)+MIN_PLAYER;
    }

    public static ArrayList<MiddleEarthCitizen> createLightArmy(){
        ArrayList<MiddleEarthCitizen> lightArmy = new ArrayList<>();
        int sizeArmy = getCountPlayer();
        System.out.println("Light army\nsize army:" + String.valueOf(sizeArmy));
        if (random.nextInt(2)==1)
        {
            lightArmy.add(new Wizard());
            --sizeArmy;
        }
        for (int i = 0; i<sizeArmy; i++){
            int j = random.nextInt(4);
            switch (j){
                case 0:
                    lightArmy.add(new Elf());
                    break;
                case 1:
                    lightArmy.add(new WoodenElf());
                    break;
                case 2:
                    lightArmy.add(new Human());
                    break;
                default:
                    lightArmy.add(new Rohhirim());
            }
        }
        return lightArmy;
    }

    public static ArrayList<MiddleEarthCitizen> createDarkArmy(){
        ArrayList<MiddleEarthCitizen> darkArmy = new ArrayList<>();
        int sizeArmy = getCountPlayer();
        System.out.println("Dark army\nsize army:" + String.valueOf(sizeArmy));
        for (int i = 0; i<sizeArmy; i++){
            int j = random.nextInt(4);
            switch (j){
                case 0:
                    darkArmy.add(new Goblin());
                    break;
                case 1:
                    darkArmy.add(new Troll());
                    break;
                case 2:
                    darkArmy.add(new UrukHai());
                    break;
                default:
                    Orc orc = new Orc();
                    orc.setWolf();
                    darkArmy.add(orc);
            }
        }
        return darkArmy;
    }

    public static void outArmy(ArrayList <MiddleEarthCitizen> army){
        army.stream()
                .map(p->p.toString())
                .forEach(str->System.out.println(str));
    }

    private static void removePlayer(ArrayList <MiddleEarthCitizen> army, List<MiddleEarthCitizen> players, MiddleEarthCitizen player){
        System.out.println(player.toString() + " погиб ");
        players.remove(player);
        army.remove(player);
    }

    private static void fight(MiddleEarthCitizen player1, ArrayList <MiddleEarthCitizen> army1, List <MiddleEarthCitizen> players1,
                              MiddleEarthCitizen player2, ArrayList <MiddleEarthCitizen> army2, List <MiddleEarthCitizen> players2){
        attack(player1,player2);
        if (player2.getPower() != 0) {
            attack(player2, player1);
            if (player1.getPower() == 0)
                removePlayer(army1,players1,player1);
        }
        else
            removePlayer(army2,players2,player2);
    }

    private static void attack(MiddleEarthCitizen who, MiddleEarthCitizen whom){
        System.out.println(who.toString() + " ударяет по " + whom.toString());
        whom.reducePower(who.getPower());
    }

    public static void roundOne(ArrayList <MiddleEarthCitizen> lightArmy,
                                ArrayList <MiddleEarthCitizen> darkArmy,
                                FilterCriteria criteria,
                                String roundName){
        if (lightArmy.size() == 0 || darkArmy.size()==0)
            return;

        List<MiddleEarthCitizen> lightPlayers;
        List<MiddleEarthCitizen> darkPlayers;
        if (criteria != null) {
            lightPlayers = lightArmy.stream()
                    .filter(p -> criteria.AcceptCriteria(p)).collect(Collectors.toList());
            darkPlayers = darkArmy.stream()
                    .filter(p -> criteria.AcceptCriteria(p)).collect(Collectors.toList());
        } else {
            lightPlayers = lightArmy;
            darkPlayers = darkArmy;
        }
        if (lightPlayers.size() == 0 && darkPlayers.size()==0) {
            System.out.println(roundName + " раунде ничья");
            return;
        }
        while (lightPlayers.size() !=0 && darkPlayers.size() !=0){
            MiddleEarthCitizen player1 = lightPlayers.get(random.nextInt(lightPlayers.size()));
            MiddleEarthCitizen player2 = darkPlayers.get(random.nextInt(darkPlayers.size()));
            if (criteria == null ? player1.getRider() : random.nextInt(2) == 1)
                fight(player1,lightArmy,lightPlayers,player2,darkArmy,darkPlayers);
            else
                fight(player2,darkArmy,darkPlayers,player1,lightArmy,lightPlayers);
        }
        if (lightPlayers.size()==0)
            System.out.println(roundName + " раунде побеждает темная армия");
        else
            System.out.println(roundName +" раунде побеждает светлая армия");
    }

}
