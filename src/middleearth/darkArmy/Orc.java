package middleearth.darkArmy;

import middleearth.MiddleEarthCitizen;

import java.io.Serializable;

/**
 * Created by Анжелика on 28.10.2018.
 */
public class Orc extends MiddleEarthCitizen {
    Wolf wolf;
    public void setWolf() {
        this.wolf = new Wolf();
    }

    @Override
    public int getPower() {
        return super.getPower() + wolf.getPower();
    }

    public int getPower(boolean isUrukHai){
        return super.getPower();
    }

    public void setWolf(Wolf wolf) {
        this.wolf = wolf;
    }
    public Orc(){
        super(random.nextInt(3)+8,"Orc" + String.valueOf(random.nextInt(99)),true);
    }

    public Orc(int power, String name, boolean isRider){
        super(power,name,isRider);
    }

    @Override
    public void reducePower(int value){
        if (value > wolf.getPower()){
            value = value - wolf.getPower();
            wolf.setPower(0);
            setPower(value > super.getPower() ? 0:  super.getPower()-value );
        }
        else
            wolf.setPower(wolf.getPower() - value);
    }

    @Override
    public String toString() {
        return super.toString() + " Wolf: " + wolf.toString();
    }

    private class Wolf implements Serializable{
        int power;
        String name;

        public String getName() {
            return name;
        }

        public int getPower() {
            return power;
        }

        public void setPower(int power) {
            this.power = power;
        }

        public Wolf(){
            this.power = random.nextInt(4) + 4;
            this.name = "Wolf" + String.valueOf(random.nextInt(99));
        }
        public String toString() {
            return "name = " + this.name + " power = " + this.power;
        }
    }


}
