package middleearth.darkArmy;

/**
 * Created by Анжелика on 28.10.2018.
 */
public class UrukHai extends Orc {
    public UrukHai(){
        super(random.nextInt(3)+10,"UrukHai" +String.valueOf(random.nextInt(99)),false);
    }

    @Override
    public int getPower() {
        return super.getPower(true);
    }

    @Override
    public void reducePower(int value) {
        setPower(value > super.getPower(true) ? 0 : super.getPower(true) - value);
    }

    @Override
    public String toString() {
        return "name: " + getName() +
                " power: "  + getPower();
    }
}
