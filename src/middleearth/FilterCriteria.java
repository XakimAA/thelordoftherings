package middleearth;

/**
 * Created by Анжелика on 28.10.2018.
 */
public interface FilterCriteria{
    boolean AcceptCriteria(MiddleEarthCitizen middleEarthCitizen);
}
