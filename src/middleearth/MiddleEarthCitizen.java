package middleearth;

import java.io.Serializable;
import java.util.Random;

/**
 * Created by Анжелика on 30.09.2018.
 */
public class MiddleEarthCitizen implements Serializable {
    protected static final Random random = new Random();
    private String name;
    private int power;
    private boolean isRider = true;

    public void setPower(int power){
        this.power = power;
    }
    public int getPower(){
        return this.power;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public boolean getRider() {
        return isRider;
    }

    public MiddleEarthCitizen(){

    }

    public MiddleEarthCitizen(int power, String name, boolean isRider){
        this.name = name;
        this.power = power;
        this.isRider = isRider;
    }

    public void reducePower(int value) {
        this.power = value > getPower() ? 0: getPower()-value;
    }

    public String toString(){
        return "name: " + name +
                " power: "  + power;
    }
}
